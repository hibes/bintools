#include <assert.h>
#include <errno.h>
#include <stdio.h>

#ifndef BINTOOLS_base16
#define BINTOOLS_base16

int base16decode(char*, int, char*, int);
int decode_nibbles(char, char, unsigned char*);

// Returns 0 on SUCCESS, non-zero otherwise.
int base16decode(char* ibuffer, int ibuffer_len, char* obuffer, int obuffer_len) {
  int ibuffer_cursor = 0;
  int obuffer_cursor = 0;
  int err_buffer_len = 256;

  char err_buffer[err_buffer_len];

  // Base 16 is two hexadecimal characters for every char spit out.
  //  if ibuffer is length 2 containing "FF", obuffer will be length one containing {0xFF}
  if (obuffer_len * 2 < ibuffer_len) {
    snprintf(err_buffer, err_buffer_len, "buffer lengths failed tests, (obuffer_len=%d * 2 < ibuffer_len=%d\n",
        obuffer_len, ibuffer_len);

    fputs(err_buffer, stderr);

    errno = EOVERFLOW;
  }

  for (; ibuffer_cursor < ibuffer_len; ibuffer_cursor+=2) {
    if (decode_nibbles(ibuffer[ibuffer_cursor], ibuffer[ibuffer_cursor + 1], (unsigned char*) &obuffer[obuffer_cursor])) {
      return -1;
    }

    obuffer_cursor += 1;
  }

  return 0;
}

// Takes two hexadecimal ascii characters [0-9A-F] and converts them into a single char
//  Returns 0 on SUCCESS, non-zero otherwise.
int decode_nibbles(char upperNibble, char lowerNibble, unsigned char* nibble) {
  *nibble = 0;

  if (upperNibble >= 48) {
    if (upperNibble >= 97 && upperNibble <= 122) {
      upperNibble -= 32;
    }

    if (upperNibble < 58) {
      *nibble += (upperNibble - 48) << 4;
    } else if (upperNibble >= 65 && upperNibble <= 70) {
      *nibble += (upperNibble - 55) << 4;
    } else {
      errno = ERANGE;

      return -1;
    }
  } else {
    errno = ERANGE;

    return -1;
  }

  if (lowerNibble >= 48) {
    if (lowerNibble >= 97 && lowerNibble <= 122) {
      lowerNibble -= 32;
    }

    if (lowerNibble < 58) {
      *nibble += (lowerNibble - 48);
    } else if (lowerNibble >= 65 && lowerNibble <= 70) {
      *nibble += (lowerNibble - 55);
    } else {
      errno = ERANGE;

      return -1;
    }
  } else {
    errno = ERANGE;

    return -1;
  }

  return 0;
}

#endif
