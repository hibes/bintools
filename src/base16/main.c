#include <base16.c>
#include <stdio.h>
#include <string.h>
#include <errno.h>

int main(int argc, char** argv) {
  // TODO: Make these arguments useful.

  int ibuffer_len = 256;
  int obuffer_len = 128;
  int err_buffer_len = 256;

  int read_length = 0;

  char ibuffer[ibuffer_len];
  char obuffer[obuffer_len];
  char err_buffer[err_buffer_len];

  errno = 0;
  read_length = fread((void *) ibuffer, sizeof(char), ibuffer_len, stdin);

  while (read_length > 0) {
    if (errno) {
      snprintf(err_buffer, err_buffer_len, "After reading from stdin, received an error strerror(errno=%d)=%s",
          errno, strerror(errno));

      fputs(err_buffer, stderr);

      return -1;
    }

    if (read_length % 2 != 0) {
      snprintf(err_buffer, err_buffer_len, "Read an unexpected odd number of bytes (%d).\n", read_length);

      fputs(err_buffer, stderr);

      return -1;
    }

    if (base16decode(ibuffer, read_length, obuffer, read_length / 2)) {
      snprintf(err_buffer, err_buffer_len, "Received an error from base16decode strerror(errno=%d)=%s.\n",
          errno, strerror(errno));

      fputs(err_buffer, stderr);

      return -1;
    }

    fwrite((void *) obuffer, sizeof(char), read_length / 2, stdout);

    read_length = fread((void *) ibuffer, sizeof(char), ibuffer_len, stdin);
  }

  return 0;
}

