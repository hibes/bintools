#include <hexdump.c>
#include <errno.h>
#include <string.h>

int main(int argc, char** argv) {
  // TODO: Make these arguments useful.

  int ibuffer_len = 128;
  int obuffer_len = CALC_OBUFFER_LEN(ibuffer_len);
  int err_buffer_len = 256;

  int read_length = 0;

  char ibuffer[ibuffer_len];
  char* obuffer;
  char err_buffer[err_buffer_len];


  obuffer = malloc(obuffer_len);

  errno = 0;
  read_length = fread((void *) ibuffer, sizeof(char), ibuffer_len, stdin);

  while (read_length > 0) {
    if (errno) {
      snprintf(err_buffer, err_buffer_len, "After reading from stdin, received an error strerror(errno=%d)=%s",
          errno, strerror(errno));

      fputs(err_buffer, stderr);
      free(obuffer);

      return -1;
    }

    if (bintools_hexdump_str(obuffer, obuffer_len, ibuffer, read_length)) {
      snprintf(err_buffer, err_buffer_len, "Received an error from base16 strerror(errno=%d)=%s.\n",
          errno, strerror(errno));

      fputs(err_buffer, stderr);
      free(obuffer);

      return -1;
    }

    fputs(obuffer, stdout);

    read_length = fread((void *) ibuffer, sizeof(char), ibuffer_len, stdin);
  }

  free(obuffer);

  return 0;
}
