#include <stdio.h>
#include <stdlib.h>

#ifndef BINTOOLS_HEXDUMP_C
#define BINTOOLS_HEXDUMP_C

// TODO: Make CALC_OBUFFER_LEN work for alternative mod and ascii values
// TODO: Make a virtual offset to solve the problem of starting the offset
//        upon each call of bintools_hexdump
// Note To Self: I'm thinking that an options struct might work better than
//  having multiple overloaded options like I currently do with mod and ascii.

#define BINTOOLS_HEXDUMP_DEFAULT_ASCII 1
#define BINTOOLS_HEXDUMP_DEFAULT_MOD   16

#define CALC_OBUFFER_LEN(L) (((((L - 1) / 16) + 1) * 85))

// Takes in a nibble_type (upper = 1, lower = 0), returns a hex representation 0-F
char nibble_to_char(char nibble, char nibble_type);

// ===Main Functions ===
// These hexdump functions basically take the ibuffer contents and convert them to
//  xxd-style output.
//
// A string of binary matching this hex representation: "000102030405060708090a0b0c0d0e0f"
// results in:
// 0000000: 00 01 02 03 04 05 06 07 08 09 0a 0b 0c 0d 0e 0f  ................
//
// ===Arguments===
// Mod is referring to the modulus used to break lines.  So if you want longer lines,
//  increase modulus from the default 16.  Beware that CALC_OBUFFER_LEN won't be accurate
//  if you do that, so you'll want to make your own calculations for size until I make
//  one.
//
// Ascii is referring to whether or not you want the ASCII text at the end to show up.
//  1 means yes, 0 means no.
char bintools_hexdump_str_mod_ascii(char* obuffer, int obuffer_len, char* ibuffer, int ibuffer_len, int mod, int ascii);
char bintools_hexdump_str_mod      (char* obuffer, int obuffer_len, char* ibuffer, int ibuffer_len, int mod);
char bintools_hexdump_str_ascii    (char* obuffer, int obuffer_len, char* ibuffer, int ibuffer_len, int ascii);
char bintools_hexdump_str          (char* obuffer, int obuffer_len, char* ibuffer, int ibuffer_len);

// These functions work the same way, except that they output to stdout instead of to
//  a char* buffer.
char bintools_hexdump_mod_ascii    (char* ibuffer, int ibuffer_len, int mod, int ascii);
char bintools_hexdump_mod          (char* ibuffer, int ibuffer_len, int mod);
char bintools_hexdump_ascii        (char* ibuffer, int ibuffer_len, int ascii);
char bintools_hexdump              (char* ibuffer, int ibuffer_len);

// Takes in a nibble_type (upper = 1, lower = 0), returns a hex representation 0-F
char nibble_to_char(char nibble, char nibble_type) {
  char nib;

  if (nibble_type != 0) {
    nib = nibble >> 4;
  } else {
    nib = nibble;
  }

  nib = nib & 0x0F;

  if (nib < 10) {
    return (char) (48 + nib);
  } else if (nib < 16) {
    return (char) (55 + nib);
  } else {
    return 0xff;
  }
}

char bintools_hexdump_str_mod_ascii(char* obuffer, int obuffer_len, char* ibuffer, int ibuffer_len, int mod, int ascii) {
  char ascii_str[mod];
  int i = 0;
  int mod_i = 0;
  int j = 0;
  int obuffer_cursor = 0;

  char fail = 0;

  int tiny_buffer_len = 11;
  char tiny_buffer[tiny_buffer_len];

  for(i = 0; i < ibuffer_len; ++i) {
    mod_i = i % mod;
    ascii_str[mod_i] = (ibuffer[i] >= 0x20 && ibuffer[i] < 0x7F ? ibuffer[i] : '.');

    if (mod_i == 0) {
      snprintf(tiny_buffer, tiny_buffer_len, "%08x: ", i);

      for (j = 0; j < tiny_buffer_len - 1; ++j) {
        obuffer[obuffer_cursor + j] = tiny_buffer[j];
      }

      obuffer_cursor += tiny_buffer_len - 1;
    }

    obuffer[obuffer_cursor] = nibble_to_char(ibuffer[i], 1);
    obuffer[obuffer_cursor + 1] = nibble_to_char(ibuffer[i], 0);
    obuffer[obuffer_cursor + 2] = ' ';
    obuffer_cursor += 3;

    if ((mod_i == mod - 1) || (i == ibuffer_len - 1)) {
      if (mod_i != mod - 1 && i == ibuffer_len - 1) {
        // Pad the string
        for (j = 0; j < mod - mod_i - 1; ++j) {
          obuffer[obuffer_cursor] = ' ';
          obuffer[obuffer_cursor + 1] = ' ';
          obuffer[obuffer_cursor + 2] = ' ';
          obuffer_cursor += 3;
        }
      }

      obuffer[obuffer_cursor] = ' ';
      obuffer[obuffer_cursor + 1] = ' ';
      obuffer_cursor += 2;

      for (j = 0; j < mod; ++j) {// - (i % mod); ++j) {
        obuffer[obuffer_cursor] = ascii_str[j];
        ++obuffer_cursor;
      }

      obuffer[obuffer_cursor] = '\n';
      ++obuffer_cursor;
    }
  }

  obuffer[obuffer_cursor] = '\0';
  ++obuffer_cursor;

  return fail;
}

char bintools_hexdump_str_mod(char* obuffer, int obuffer_len, char* ibuffer, int ibuffer_len, int mod) {
  return bintools_hexdump_str_mod_ascii(obuffer, obuffer_len, ibuffer, ibuffer_len, mod, BINTOOLS_HEXDUMP_DEFAULT_ASCII);
}

char bintools_hexdump_str_ascii(char* obuffer, int obuffer_len, char* ibuffer, int ibuffer_len, int ascii) {
  return bintools_hexdump_str_mod_ascii(obuffer, obuffer_len, ibuffer, ibuffer_len, BINTOOLS_HEXDUMP_DEFAULT_MOD, ascii);
}

char bintools_hexdump_str(char* obuffer, int obuffer_len, char* ibuffer, int ibuffer_len) {
  return bintools_hexdump_str_mod_ascii(obuffer, obuffer_len, ibuffer, ibuffer_len,
      BINTOOLS_HEXDUMP_DEFAULT_MOD, BINTOOLS_HEXDUMP_DEFAULT_ASCII);
}

char bintools_hexdump_mod_ascii(char* ibuffer, int ibuffer_len, int mod, int ascii) {
  char* obuffer;
  int obuffer_len = CALC_OBUFFER_LEN(ibuffer_len);

  char fail = 0;

  obuffer = malloc(obuffer_len);

  fail = bintools_hexdump_str_mod_ascii(obuffer, obuffer_len, ibuffer, ibuffer_len, mod, ascii);
  printf("%s\n", obuffer);

  free(obuffer);

  return fail;
}

char bintools_hexdump_mod(char* ibuffer, int ibuffer_len, int mod) {
  return bintools_hexdump_mod_ascii(ibuffer, ibuffer_len, mod, BINTOOLS_HEXDUMP_DEFAULT_ASCII);
}

char bintools_hexdump_ascii(char* ibuffer, int ibuffer_len, int ascii) {
  return bintools_hexdump_mod_ascii(ibuffer, ibuffer_len, BINTOOLS_HEXDUMP_DEFAULT_MOD, ascii);
}

char bintools_hexdump(char* ibuffer, int ibuffer_len) {
  return bintools_hexdump_mod_ascii(ibuffer, ibuffer_len, BINTOOLS_HEXDUMP_DEFAULT_MOD, BINTOOLS_HEXDUMP_DEFAULT_ASCII);
}

#endif
