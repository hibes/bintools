#!/bin/bash

# This script will standardize the build process and produce no standard output unless a failure occurs.

yell() { echo "$0: $*" >&2; }

MAKE_LOG=make.log

if [ -f "$MAKE_LOG" ]; then
  rm "$MAKE_LOG"
fi

touch $MAKE_LOG

set -o pipefail

# Redirect stderr to stdout,
make 2>&1 >> "$MAKE_LOG" | tee -a "$MAKE_LOG"

error=$?

if [ $error -ne 0 ]; then
  cat "$MAKE_LOG"

  yell "Make returned error=$error, see log output above^"
else
  yell "Build was successful."
fi
