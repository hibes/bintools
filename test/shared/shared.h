#ifndef BINTOOLS_SHARED
#define BINTOOLS_SHARED

#define PASS(X) printf("PASSED! %s {%s: %s() Line: %d}\n", X, __FILE__, __FUNCTION__, __LINE__);
#define FAIL(X) printf("FAILED! %s {%s: %s() Line: %d}\n", X, __FILE__, __FUNCTION__, __LINE__);

#endif
