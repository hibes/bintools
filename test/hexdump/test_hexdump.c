#include <hexdump.c>
#include <shared.h>
#include <stdio.h>
#include <stdlib.h>

#ifndef BINTOOLS_TEST_HEXDUMP
#define BINTOOLS_TEST_HEXDUMP

int test_nibble_to_char() {
  int i;
  char test_nibble_to_char_array[16] = "0123456789ABCDEF";
  char n2c;

  char fail = 0;

  int buffer_len = 255;
  char buffer[buffer_len];

  int successes = 0;
  int failures  = 0;

  for (i = 0; i < 16; ++i) {
    n2c = nibble_to_char((char) (i & 0xF), 0);

    snprintf(buffer, buffer_len, "%c == %c", test_nibble_to_char_array[i], n2c);

    if (test_nibble_to_char_array[i] == n2c) {
      ++successes;
#ifdef VERBOSE_TESTING
      PASS(buffer);
#endif
    } else {
      ++failures;
      FAIL(buffer);
      fail = 1;
    }
  }

  char nibble = 0xfe;

  n2c = nibble_to_char(nibble, 0);

  snprintf(buffer, buffer_len, "%c == %c", 'E', n2c);

  if ('E' == n2c) {
    ++successes;
#ifdef VERBOSE_TESTING
    PASS(buffer);
#endif
  } else {
    ++failures;
    FAIL(buffer);
  }

  n2c = nibble_to_char(nibble, 1);

  snprintf(buffer, buffer_len, "%c == %c", 'F', n2c);

  if ('F' == n2c) {
    ++successes;
#ifdef VERBOSE_TESTING
    PASS(buffer);
#endif
  } else {
    ++failures;
    FAIL(buffer);
  }

  snprintf(buffer, buffer_len, "%d/%d successes.", successes, successes + failures);

  if (fail == 0) {
    PASS(buffer);
  } else {
    FAIL(buffer);
  }

  return fail;
}

int test_bintools_hexdump() {
  int i;

  char fail = 0;

  char test0[0];
  char test1[1];
  char test2[2];
  char test15[15];
  char test16[16];
  char test256[256];

  for (i = 0; i < 256; ++i) {
    if (i < 16) {
      if (i < 15) {
        test15[i] = i;
      }

      test16[i] = i;
    }

    test256[i] = i;
  }

  test1[0] = '\0';
  test2[0] = '0';
  test2[1] = '\0';
  test15[14] = '\0';
  test16[15] = '\0';
  test256[255] = '\0';

  printf("\n\nTest0:\n");
  bintools_hexdump(test0, 0);
  printf("\n\nTest1:\n");
  bintools_hexdump(test1, 1);
  printf("\n\nTest2:\n");
  bintools_hexdump(test2, 2);
  printf("\n\nTest15:\n");
  bintools_hexdump(test15, 15);
  printf("\n\nTest16:\n");
  bintools_hexdump(test16, 16);
  printf("\n\nTest256:\n");
  bintools_hexdump(test256, 256);

  return fail;
}

#endif
