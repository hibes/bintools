#include <test_hexdump.c>

int main() {
  int fail = 0;

  fail += test_nibble_to_char();
  // This test isn't ready yet, so I'm commenting it out for this commit.
  //fail += test_bintools_hexdump();

  return fail;
}
