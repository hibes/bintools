Bintools Repo
==============

Description
--------------
The basic layout for the tests resemble this:

Expects file tree like so:

    | root {the root directory containing the rest of these files.
    |-- build
    |   |-- tests
    |       |-- {collection of binary test routines not needing args}
    |-- test
        |-- run_tests.sh {this file}
        |-- {project_folder}
            |-- run_test.sh {shell script that runs some tests for the project}

Instructions
--------------
TESTS_OUT=build/tests {or alternatively an other output directory that 
  contains a bunch of binary test routines to run without args}

You can use "make" to build all the projects and run all the tests.

TODO
--------------
- Automate more tests

