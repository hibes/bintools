#!/bin/bash

# Expects file tree like so:
#  TESTS_OUT=build/tests {or alternatively an other output directory that
#   contains a bunch of binary test routines to run without args}
# | root {the root directory containing the rest of these files.
# |-- build
# |   |-- tests
# |       |-- {collection of binary test routines not needing args}
# |-- test
#     |-- run_tests.sh {this file}
#     |-- {project_folder}
#         |-- run_test.sh {shell script that runs some tests for the project}

export TEST_SCRIPTS_DIR=test/*
export TEST_SCRIPTS_FNAME=run_test.sh
export TEST_DATA_DIR=test/data
export TESTS_OUT

. test/helper_funcs.sh

# Run each of the compiled binary tests.
for test in $TESTS_OUT/*; do
  $test
done

# Run each of the test scripts.
for test_script in $(find $TESTS_SCRIPTS_DIR -name "$TEST_SCRIPTS_FNAME"); do
  $test_script
done

