#!/bin/bash

function PASS() {
  echo -ne "PASSED! $4 {$1: Line: $2 Project: $3}\n"
}

function FAIL() {
  echo -ne "FAILED! $4 {$1: Line: $2 Project: $3}\n"
}

# Accepts a quoted byte stream and prints out hex
bin2hex() {
  echo -ne "$1" | hexdump
}
