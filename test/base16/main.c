#include <test_base16.c>
#include <getopt.h>

void usage();

int main(int argc, char** argv) {
  char fail = 0;
  int option_index = 0;

  // As many as each argument could be a file to process.
  char* files[argc];
  int files_cursor = 0;
  int files_count = 0;

  verbose = 0;

  if (argc == 1) {
    fail += test_decode_nibbles();
  } else {
    int c;

    static struct option long_options[] = {
      {"verbose", no_argument,        0,  'v'},
      {"help",    no_argument,        0,  'h'},
      {"file",    required_argument,  0,  'f'},
      {0,         0,                  0,  0}
    };

    while ((c = getopt_long(argc, argv, "vhf:", long_options, &option_index)) != -1) {
      switch(c) {
        case 'v':
          verbose = 1;

          break;
        case 'f':
          files[files_cursor] = malloc(strlen(optarg) + 1);
          files[files_cursor][0] = '\0';
          strcpy(files[files_cursor], optarg);

          files_count = ++files_cursor;

          break;
        case 'h': // fallthrough to usage
        default:

          usage(argv, option_index);

          return -1;
      }
    }

    if (files_count == 0) {
      usage(argv, -1);

      return -1;
    }

    for (files_cursor = 0; files_cursor < files_count; ++files_cursor) {
      fail += test_decode_sample_file(files[files_cursor]);
    }
  }

  for (files_cursor = 0; files_cursor < files_count; ++files_cursor) {
    free(files[files_cursor]);
  }

  return fail;
}

void usage(char** argv, int option_index) {
  //TODO Rewrite usage to be accurate.
  fprintf(stderr, "%s - Usage\n", argv[0]);
  if (option_index > 0) {
    fprintf(stderr, "argv[i=%d]=%s got you here...\n", option_index, argv[option_index]); 
  }
  fprintf(stderr, "\n");
  fprintf(stderr, "This testing tool expects a list of filenames containing binary data.  ");
  fprintf(stderr, "For each file, there should be an accompanying .base16 file which "     );
  fprintf(stderr, "contains the base16 encoded version of that file.  This tool will try " );
  fprintf(stderr, "to test the base16encode function applied on the base16 encoded file "  );
  fprintf(stderr, "and verify it using the binary file.\n");
}

