#!/bin/bash

# Expects file tree like so:
#  TESTS_OUT=build/tests {or alternatively an other output directory that
#   contains a bunch of binary test routines to run without args}
# | root {the root directory containing the rest of these files.
# |-- build
# |   |-- tests
# |       |-- {collection of binary test routines not needing args}
# |-- test
#     |-- run_tests.sh {this script runs all the sub-scripts (run_test.sh)}
#     |-- {project_folder}
#         |-- run_test.sh {this shell script which runs some tests for the project}

. test/helper_funcs.sh

PROJECT=base16
DATA=$TEST_DATA_DIR/$PROJECT

files=

for i in $(find $DATA -type f | grep -v "\.base16$"); do
  files="--file=$i $files"
done

$TESTS_OUT/test_${PROJECT}${EXE} $files



val=$(echo -ne "uuuu" | base16 -d 2>&1)

if [[ "$val" == *"strerror(errno=34)=Numerical result out of range"* ]]; then
  PASS $BASH_SOURCE $LINENO $PROJECT "base16 failed as expected on non-hexadecimal characters."
else
  echo "$val"
  FAIL $BASH_SOURCE $LINENO $PROJECT "$val"
fi
