#include <base16.c>
#include <hexdump.c>
#include <shared.h>
#include <stdio.h>
#include <errno.h>
#include <string.h>

#ifndef BINTOOLS_TEST_base16
#define BINTOOLS_TEST_base16

#define ENV_VAR_TEST_DATA_DIR "TEST_DATA_DIR"

char verbose = 0;

char test_decode_nibbles() {
  char fail = 0;

  int failures = 0;
  int successes = 0;

  int buffer_len = 256;
  char buffer[buffer_len];

  int err_buffer_len = 256;
  char err_buffer[err_buffer_len];

  unsigned char combinations = 255;

  unsigned char i;
  unsigned char decoded;

  char upper, lower;

  for (i = 0; i <= combinations; ++i) {
    upper = nibble_to_char(i, 1);
    lower = nibble_to_char(i, 0);

    if (decode_nibbles(upper, lower, &decoded)) {
      snprintf(err_buffer, err_buffer_len, "Received an error from decode_nibbles strerror(errno=%d)=%s.\n",
          errno, strerror(errno));

      fputs(err_buffer, stderr);

      return -1;
    }

    if (decoded != i) {
      ++fail;
      ++failures;

      snprintf(
          buffer,
          buffer_len,
          "i=0x%x (%d) upper=%c (0x%x %d) lower=%c (0x%x %d) decoded=0x%x (%d)",
          i, i,
          upper, upper, upper,
          lower, lower, lower,
          decoded, decoded);

      FAIL(buffer);
    } else {
      ++successes;

#ifdef VERBOSE_TESTING
      snprintf(
          buffer,
          buffer_len,
          "i=0x%x (%d) upper=%c (0x%x %d) lower=%c (0x%x %d) decoded=0x%x (%d)",
          i, i,
          upper, upper, upper,
          lower, lower, lower,
          decoded, decoded);

      PASS(buffer);
#endif
    }

    if (i == 255) {
      break;
    }
  }

  snprintf(buffer, buffer_len, "%d/%d successes.", successes, combinations);

  if (fail == 0) {
    PASS(buffer);
  } else {
    FAIL(buffer);
  }

  return fail;
}

char test_decode_sample(char* sample_base16, char* sample) {
  char fail = 0;

  // 256 bits of sample_base16 maps to 128 bits of sample
  //  and I don't need the \0 bit if I read as raw bytes
  //  instead of text.
  int sample_base16_buffer_len = 256;
  int sample_buffer_len = 128;
  int base16_output_buffer_len = 128;

  // We'll use these temp values to only use as much of the
  //  buffer as we read.
  int temp_sample_base16_buffer_len = 0;
  int temp_sample_buffer_len = 0;
  int expected_sample_buffer_len = 0;

  // Track the total number of bytes read.
  int cumulative_read = 0;

  // Track the offset start of the last read.
  int sample_base16_offset = 0;
  int sample_offset = 0;

  char sample_base16_buffer[sample_base16_buffer_len];
  char sample_buffer[sample_buffer_len];

  char base16_output_buffer[base16_output_buffer_len];

  FILE * sample_base16_file;
  FILE * sample_file;

  int err_buffer_len = 256;
  char err_buffer[err_buffer_len];

  // Using this to make sure we bypass the while loop and still close the files we opened.
  int cont = 1;

  errno = 0;

  sample_base16_file = fopen(sample_base16, "r");

  if (sample_base16_file == (FILE*) -1) {
    snprintf(err_buffer, err_buffer_len, "Failed to open file %s for sampling", sample_base16);

    fail += 1;

    cont = 0;
  } else if (errno != 0) {
    snprintf(err_buffer, err_buffer_len, "fopen(%s, \"r\") caused strerror(errno=%d)=%s",
        sample_base16, errno, strerror(errno));

    errno = 0;
    fail += 1;

    cont = 0;
  } else if (sample_base16_file == NULL) {
    snprintf(err_buffer, err_buffer_len, "NullPointer open sample_base16_file (%s)", sample);

    fail += 1;

    cont = 0;
  }

  if (fail == 0) {
    errno = 0;

    sample_file = fopen(sample, "r");
  } else {
    sample_file = 0;
  }

  if (sample_file == (FILE*) -1) {
    snprintf(err_buffer, err_buffer_len, "Failed to open file %s for verifying the sample", sample);

    fail += 1;

    cont = 0;
  } else if (errno != 0) {
    snprintf(err_buffer, err_buffer_len, "fopen(%s, \"r\") caused strerror(errno=%d)=%s",
        sample, errno, strerror(errno));

    errno = 0;
    fail += 1;

    cont = 0;
  } else if (sample_file == NULL) {
    snprintf(err_buffer, err_buffer_len, "NullPointer open sample_file (%s)", sample_base16);

    fail += 1;

    cont = 0;
  }

  while (cont) {
    // If we've already hit the EOF, we don't want to call fread again.
    if (feof(sample_base16_file)) {
      break;
    }

    temp_sample_base16_buffer_len = fread(sample_base16_buffer, sizeof(char), sample_base16_buffer_len, sample_base16_file);

    //printf("Read %d of planned %d bytes at offset %d (0x%x) from sample_base16_file %s\n",
    //    temp_sample_base16_buffer_len, sample_base16_buffer_len, (int) sample_base16_offset, (int) sample_base16_offset, sample_base16);

    cumulative_read += temp_sample_base16_buffer_len;

    if (temp_sample_base16_buffer_len % 2 != 0) {
      snprintf(err_buffer, err_buffer_len, "Unexpectedly read an odd number of bytes (%d) from file %s",
          temp_sample_base16_buffer_len, sample_base16);

      fail += 1;

      break;
    }

    if (temp_sample_base16_buffer_len == 0) {
      if (feof(sample_base16_file) != 0) {
        break;
      } else {
        snprintf(err_buffer, err_buffer_len, "Received an error (%d: %s) while reading the sample_base16 file: %s",
            ferror(sample_base16_file), strerror(errno), sample_base16);

        fail += 1;

        break;
      }
    } else {
      expected_sample_buffer_len = temp_sample_base16_buffer_len / 2;

      temp_sample_buffer_len = fread(sample_buffer, sizeof(char), expected_sample_buffer_len, sample_file);

      //printf("Read %d of planned %d bytes at offset %d (0x%x) from sample_file %s\n",
      //    temp_sample_buffer_len, expected_sample_buffer_len, (int) sample_offset, (int) sample_offset, sample);

      sample_offset += temp_sample_buffer_len;

      if (temp_sample_buffer_len == 0) {
        if (feof(sample_file) != 0) {
          // EOF found early
          snprintf(err_buffer, err_buffer_len, "EOF found early while reading the sample file: %s", sample);

          fail += 1;

          break;
        } else {
          snprintf(err_buffer, err_buffer_len, "Received an error (%d: %s) while reading the sample file: %s",
              ferror(sample_file), strerror(errno), sample);

          fail += 1;

          break;
        }
      }

      base16decode(sample_base16_buffer, temp_sample_base16_buffer_len, base16_output_buffer, temp_sample_buffer_len);

      if (memcmp(base16_output_buffer, sample_buffer, temp_sample_buffer_len) != 0) {
        snprintf(err_buffer, err_buffer_len, "Memory comparison failed at offset %d (0x%x) (%s)",
            sample_base16_offset, sample_base16_offset, sample_base16);

        fail += 1;

        if (verbose) {
          printf("base16_output_buffer\n");
          bintools_hexdump(base16_output_buffer, base16_output_buffer_len);
          printf("sample_buffer\n");
          bintools_hexdump(sample_buffer, base16_output_buffer_len);
          printf("sample_base16_buffer\n");
          bintools_hexdump(sample_base16_buffer, base16_output_buffer_len * 2);
        }

        break;
      }
    }

    sample_base16_offset += temp_sample_base16_buffer_len;
  }

  if (sample_base16_file != NULL) {
    if (fclose(sample_base16_file)) {
      snprintf(err_buffer, err_buffer_len, "Failed to close the sample.base16 file %s", sample_base16);
    }
  }

  if (sample_file != NULL) {
    if (fclose(sample_file)) {
      snprintf(err_buffer, err_buffer_len, "Failed to close the sample file %s", sample);
    }
  }

  if (fail == 0) {
    snprintf(err_buffer, err_buffer_len, "Successfully read, decoded, and verified %d bytes from file %s",
        cumulative_read, sample_base16);
    PASS(err_buffer);
  } else {
    FAIL(err_buffer);
  }

  sample_base16_file = 0;
  sample_file = 0;

  return fail;
}

char test_decode_sample_file(char* filename) {
  int sample_base16_len;
  char* sample_base16;

  char base16_extension[] = ".base16";

  char fail = 0;

  sample_base16_len = strlen(filename) + strlen(base16_extension) + 1;

  sample_base16 = malloc(sample_base16_len);
  sample_base16[0] = '\0';

  strcat(sample_base16, filename);
  strcat(sample_base16, base16_extension);

  fail += test_decode_sample(sample_base16, filename);

  free(sample_base16);

  sample_base16 = 0;

  return fail;
}

#endif
