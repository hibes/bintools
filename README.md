Bintools Repo
==============

Description
--------------
This **bintools** repo exists to provide some basic binary manipulation toolsets in C.

Instructions
--------------
You can use "make" to build all the projects and run all the tests.

Tests
--------------
You can find [automated test information here](test/Readme.md).

TODO
--------------
- Automate more tests
- base16encode
 - combine base16encode and base16decode into a single binary with args.
- base64 encode/decode
