# Henry Brown
# 2015
#
# bintools_lib will build a library
# bins will build all of the binaries (hexdump, base16, etc.)
# all will build everything, run tests, and install (copy to $HOME/bin/)

CC=gcc
RM=rm
MKDIR=mkdir

#CFLAGS=-Wall -g
CFLAGS=-Wall

BUILD_DIR=build
EXE=
EXE_OUT=$(BUILD_DIR)
OBJ_OUT=$(BUILD_DIR)/obj
TESTS_OUT=$(BUILD_DIR)/tests
HOME_BIN=~/bin
LIB=.so
STATIC=-static

TEST_SCRIPT=test/run_tests.sh

DEP=mkdirs

BASE16_BIN=$(EXE_OUT)/base16$(EXE)
HEXDUMP_BIN=$(EXE_OUT)/hexdump$(EXE)

BASE16_O=$(OBJ_OUT)/base16.o
HEXDUMP_O=$(OBJ_OUT)/hexdump.o

INCLUDE_DIRS=-I src/hexdump -I src/base16 -I test/shared -I test/base16 -I test/hexdump

all: $(DEP) bins run_tests install clean_objs

mkdirs:
	$(MKDIR) -p $(BUILD_DIR)
	$(MKDIR) -p $(EXE_OUT)
	$(MKDIR) -p $(OBJ_OUT)
	$(MKDIR) -p $(TESTS_OUT)
	$(MKDIR) -p $(HOME_BIN)

bins: $(DEP) bintools_lib hexdump base16

bintools_lib: $(DEP) hexdump.o base16.o
	ar rcs build/lib_bintools$(LIB) $(HEXDUMP_O) $(BASE16_O)

hexdump: $(DEP) hexdump.o
	$(CC) $(CFLAGS) $(INCLUDE_DIRS) src/hexdump/main.c -o $(HEXDUMP_BIN)

hexdump.o: $(DEP)
	$(CC) $(CFLAGS) $(STATIC) -c src/hexdump/hexdump.c -o $(HEXDUMP_O)

base16: $(DEP) base16.o
	$(CC) $(CFLAGS) $(INCLUDE_DIRS) src/base16/main.c -o $(BASE16_BIN)

base16.o: $(DEP)
	$(CC) $(CFLAGS) $(STATIC) -c src/base16/base16.c -o $(BASE16_O)

tests: $(DEP) bins test_hexdump test_base16

test_hexdump: $(DEP)
	$(CC) $(CFLAGS) $(INCLUDE_DIRS) test/hexdump/main.c -o $(TESTS_OUT)/test_hexdump$(EXE)

test_base16: $(DEP)
	$(CC) $(CFLAGS) $(INCLUDE_DIRS) test/base16/main.c -o $(TESTS_OUT)/test_base16$(EXE)

run_tests: $(DEP) tests
	EXE=$(EXE) TESTS_OUT=$(TESTS_OUT) $(TEST_SCRIPT)

install: $(DEP) bins
	find $(shell pwd)/$(EXE_OUT) -maxdepth 1 -type f -executable -exec cp {} $(HOME_BIN)/ \;

clean_objs:
	@ $(RM) -rf $(OBJ_OUT)

clean:
	$(RM) -rf $(BUILD_DIR)/*
